unsigned long lastTimes[NUM_SUBDIVISIONS];

void handleTick(unsigned long tick,bool ticks[NUM_SUBDIVISIONS]) {
 for(int t=0; t<NUM_SUBDIVISIONS; t++){
    float subDivTempo = subdivisions[t]*tempo;
    /*
    Serial.print(subDivTempo);
    
    
    Serial.print("-");
     Serial.print(tempo);
     Serial.println(" bpm");
     */
     
    if (tick - lastTimes[t] >= subDivTempo) {
      ticks[t] = 1;
      lastTimes[t] = tick;
    }
    else{
      ticks[t] = 0;
    }
 
  }

  if(ticks[7] == 1){
    tempoBlinkValue = 50;
  
    }
    else{
       tempoBlinkValue = tempoBlinkValue*0.9;
       if(tempoBlinkValue <0){
        tempoBlinkValue =0;
        }
      }
}

void updateTempo(){

  tempo = (60.00 * 1000)/ parameterClasses[TEMPO_PARAM_INDEX].currentValue; 
  
  Serial.print("update tempo to");
  Serial.print(parameterClasses[TEMPO_PARAM_INDEX].currentValue);
  Serial.print(" - ");
  Serial.println(tempo);  
 }
