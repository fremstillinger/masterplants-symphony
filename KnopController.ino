Rotary knobLeft = Rotary(KNOP_LEFT_1, KNOP_LEFT_2);
Rotary knobRight = Rotary(KNOP_RIGHT_1, KNOP_RIGHT_2);
Bounce rightButton;
Bounce leftButton;

void setupKnops(){
  
   pinMode(KNOP_RIGHT_BTN, INPUT_PULLUP);
  rightButton.attach(KNOP_RIGHT_BTN);
  rightButton.interval(5);

   pinMode(KNOP_LEFT_BTN, INPUT_PULLUP);
  leftButton.attach(KNOP_LEFT_BTN);
  leftButton.interval(5);
  
}


void updateKnops(){
  
   unsigned char knopLeftResult = knobLeft.process();
  if (knopLeftResult) {

    if (knopLeftResult == DIR_CW) {
      leftKnopTurned(1);
    }
    else {
      leftKnopTurned(-1);
    }

  }

  unsigned char knopRightResult = knobRight.process();
  if (knopRightResult) {

    if (knopRightResult == DIR_CW) {
      rightKnopTurned(1);
    }
    else {
      rightKnopTurned(-1);
    }
  }
  
  if (rightButton.update()) {
    if (rightButton.read()) {
      rightBtnPressed();
    }
  }


   if (leftButton.update()) {
    if (!leftButton.read()) {
      leftBtnPressed();
    }
    else{
       leftBtnReleased();
      }
  }
  
  
  }
void leftKnopTurned(int val) {
  if (val == 1) {
    if (selectedParameter < NUM_PARAMS - 1) {

      selectedParameter ++;
    }
  }

  if (val == -1) {
    if (selectedParameter > 0) {
      selectedParameter --;
    }
  }
}

void rightKnopTurned(int val) {

  if (val == 1) {
    parameterClasses[selectedParameter].currentValue =  parameterClasses[selectedParameter].currentValue + parameterClasses[selectedParameter].resolution;
    
    if (parameterClasses[selectedParameter].currentValue >= parameterClasses[selectedParameter].maxValue) {
      parameterClasses[selectedParameter].currentValue = parameterClasses[selectedParameter].maxValue;
    }
  }

  if (val == -1) {
    parameterClasses[selectedParameter].currentValue =  parameterClasses[selectedParameter].currentValue - parameterClasses[selectedParameter].resolution;
    
    if (parameterClasses[selectedParameter].currentValue <= parameterClasses[selectedParameter].minValue) {
      parameterClasses[selectedParameter].currentValue = parameterClasses[selectedParameter].minValue;
    }
  }
 
  switch (selectedParameter) {
    case TEMPO_PARAM_INDEX:
    updateTempo();
    break;
    case PLANT_SELECT_PARAM_INDEX: // select mode, do nothing
    break;
    
    default:
      for (int p = 0; p < NUM_PLANTS; p++) {
        if (plantClasses[p].isSelected()) {
          plantClasses[p].setParameterValue(selectedParameter, parameterClasses[selectedParameter].currentValue); //   [selectedParameter] = parameters[selectedParameter][4];
        }
      }
  }
}

void rightBtnPressed() {
  // if in select plant mode
  if (selectedParameter == 0) {
    plantClasses[parameterClasses[selectedParameter].currentValue].toogleSelection();
    
    /*
    if (plants[parameters[selectedParameter][4]][0]) {
      plants[parameters[selectedParameter][4]][0] = 0;
    }
    else {
      plants[parameters[selectedParameter][4]][0] = 1;
    }
    */
  }
}

int _prevSelectedParameter = 0;

void leftBtnPressed() {
  _prevSelectedParameter = selectedParameter;
  selectedParameter = PLANT_SELECT_PARAM_INDEX;
}

void leftBtnReleased() {
  selectedParameter = _prevSelectedParameter;
}
