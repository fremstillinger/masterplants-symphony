#include "FastLED.h"
#include "Plant.h"
#include "Parameter.h"
#include "Bounce2.h"
#include "Timer.h"
#include <Rotary.h>
#include "GlobalVariables.h"
#include <MIDI.h>

MIDI_CREATE_INSTANCE(HardwareSerial, Serial1, MIDI_MASTER);
MIDI_CREATE_INSTANCE(HardwareSerial, Serial2, MIDI_SEPARATE);

bool ticks[NUM_SUBDIVISIONS];

Timer t;
int tempoBlinkValue = 0;
unsigned long tempo = 6000 / 10;

int selectedParameter = 0;
Parameter parameterClasses[NUM_PARAMS];
Plant plantClasses[NUM_PLANTS];

int defaultParameterValues[NUM_PARAMS];
int plantInputs[NUM_PLANTS] = {A8, A9, A10, A11, A12, A13, A14, A15};

bool tick = false;
int selectedValue = 0;
bool blinkState = 0;



void setup() {

  pinMode(MULTIPLEXER_PIN_A, OUTPUT);
  pinMode(MULTIPLEXER_PIN_B, OUTPUT);
  pinMode(MULTIPLEXER_PIN_C, OUTPUT);


  /* int llLight,int rlLight,int type,int resolution,int currentValue,int minVal,int maxVal */
  /* is selected */

  parameterClasses[PLANT_SELECT_PARAM_INDEX].setupParameter(1, 1, 3, 1, 0, 0, NUM_PLANTS - 1);
  /*  select tone */
  parameterClasses[PLANT_TONE_INDEX].setupParameter(2, 2, 5, 1, 0, 0, 11);
  /* select scale */
  parameterClasses[PLANT_SCALE_INDEX].setupParameter(3, 3, 5, 1, 4, 0, NUM_SCALES - 1);
  /* tone min */
  parameterClasses[TONE_MIN_PARAM_INDEX].setupParameter(4, -1, 0, 1, 30, 0, MAX_VALUE);
  /* tone max */
  parameterClasses[TONE_MAX_PARAM_INDEX].setupParameter(-1, 4, 0, 1, 80, 0, MAX_VALUE);
  /* tone subdivision */
  parameterClasses[SUBDIV_PARAM_INDEX].setupParameter(6, 6, 5, 1, 0, 0, NUM_SUBDIVISIONS - 1);
  /* tempo */
  parameterClasses[TEMPO_PARAM_INDEX].setupParameter(7, -1, 4, 1, 120, 20, 179);


  MIDI_MASTER.begin(MIDI_CHANNEL_OMNI);
  MIDI_SEPARATE.begin(MIDI_CHANNEL_OMNI);

  Serial.begin(115200);
  setupLedStrips();
  t.every(300, changeBlinkState);

  setupKnops();

  updateTempo();
  
  // set all plants to default parameter values
  for (int p = 0; p < NUM_PARAMS; p++) {
    defaultParameterValues[p] = parameterClasses[p].currentValue;
  }


  for (int p = 0; p < NUM_PLANTS; p++) {
    plantClasses[p].setupPlant(p, plantInputs[p], defaultParameterValues);
    if (p == 0) {
      plantClasses[p].toogleSelection();
    }
  }
}

void loop() {
  updateKnops();
  t.update();
  updateLeds();

  handleTick(millis(), ticks);

  for (int pl = 0; pl < NUM_PLANTS; pl++) {
    plantClasses[pl].doReading();

    if (!plantClasses[pl].isActive()) {
      continue;
    }

    if (ticks[plantClasses[pl].getParameterValue(SUBDIV_PARAM_INDEX)]) {

#ifdef  DEBUG_MODE

      Serial.print("Plant no: ");
      Serial.print( plantClasses[pl].getPlantIndex() + 1);
      Serial.print(" plays ");
      Serial.println(plantClasses[pl].getCurrentTone());

#endif

      int currentTone = plantClasses[pl].getCurrentTone();
      int prevTone = plantClasses[pl].getPrevTone();

      if (currentTone != prevTone) {


        MIDI_MASTER.sendNoteOff(prevTone, 127, plantClasses[pl].getPlantIndex() + 1);
        MIDI_MASTER.sendNoteOn(currentTone, 127, plantClasses[pl].getPlantIndex() + 1);


        switch (pl + 1) {
          case 1:
            digitalWrite(MULTIPLEXER_PIN_A, LOW);
            digitalWrite(MULTIPLEXER_PIN_B, LOW);
            digitalWrite(MULTIPLEXER_PIN_C, LOW);


            break;

          case 2:

            digitalWrite(MULTIPLEXER_PIN_C, LOW);
            digitalWrite(MULTIPLEXER_PIN_B, LOW);
            digitalWrite(MULTIPLEXER_PIN_A, HIGH);


            break;

          case 3:

            digitalWrite(MULTIPLEXER_PIN_C, LOW);
            digitalWrite(MULTIPLEXER_PIN_B, HIGH);
            digitalWrite(MULTIPLEXER_PIN_A, LOW);

            break;

          case 4:

            digitalWrite(MULTIPLEXER_PIN_C, LOW);
            digitalWrite(MULTIPLEXER_PIN_B, HIGH);
            digitalWrite(MULTIPLEXER_PIN_A, HIGH);

            break;

          case 5:

            digitalWrite(MULTIPLEXER_PIN_C, HIGH);
            digitalWrite(MULTIPLEXER_PIN_B, LOW);
            digitalWrite(MULTIPLEXER_PIN_A, LOW);

            break;

          case 6:

            digitalWrite(MULTIPLEXER_PIN_C, HIGH);
            digitalWrite(MULTIPLEXER_PIN_B, LOW);
            digitalWrite(MULTIPLEXER_PIN_A, HIGH);

            break;

          case 7:

            digitalWrite(MULTIPLEXER_PIN_C, HIGH);
            digitalWrite(MULTIPLEXER_PIN_B, HIGH);
            digitalWrite(MULTIPLEXER_PIN_A, LOW);

            break;

          case 8:

            digitalWrite(MULTIPLEXER_PIN_C, HIGH);
            digitalWrite(MULTIPLEXER_PIN_B, HIGH);
            digitalWrite(MULTIPLEXER_PIN_A, HIGH);

            break;

        }

        MIDI_SEPARATE.sendNoteOff(prevTone, 127, 1);
        MIDI_SEPARATE.sendNoteOn(currentTone, 127, 1);
        plantClasses[pl].setPrevTone(currentTone);
      }
    }
  }
}




