/* led arrays */
CRGB paramLedsLeft[NUM_PARAM_LEDS];
CRGB paramLedsRight[NUM_PARAM_LEDS];
CRGB stepLeds[NUM_STEP_LEDS];

/* colors */
CRGB orange = CRGB(70, 10, 0);
CRGB valueColor = CRGB(10, 10, 0);
CRGB white = CRGB(15, 10, 5);
CRGB countColor = CRGB(20, 2, 2);
CRGB plantGreen = CRGB(0, 2, 0);
CRGB inactive = CRGB(0, 0, 0);
CRGB blinkColor = CRGB(30, 30, 30);

void setupLedStrips() {
  FastLED.addLeds<NEOPIXEL, PARAMS_PIN_LEFT>(paramLedsLeft, NUM_PARAM_LEDS);
  FastLED.addLeds<NEOPIXEL, PARAMS_PIN_RIGHT>(paramLedsRight, NUM_PARAM_LEDS);
  FastLED.addLeds<NEOPIXEL, STEPS_PIN>(stepLeds, NUM_STEP_LEDS);
}


void updateLeds() {

  int parameterMin = parameterClasses[selectedParameter].minValue;
  int parameterMax = parameterClasses[selectedParameter].maxValue;
  int parameterSpan = (parameterMax + 1) - parameterMin;
  int parameterVal =  parameterClasses[selectedParameter].currentValue;

  float steps = (parameterVal - parameterMin) / (parameterSpan * 1.00) * NUM_STEP_LEDS;
  int mainSteps = steps;
  int subSteps = (steps * 10 - mainSteps * 10);

  /* clear all colors */
  for (int i = 0; i < NUM_STEP_LEDS; i++) {
    stepLeds[i] = inactive;
  }



  // // type (0=step, 1=min,2=max,3=select,4=numeric,5=absolute step)

  int parameterType = parameterClasses[selectedParameter].type;

  renderParameterSelection();

  // color selected plant leds
  renderPlantSelction();

  for(int pl=0; pl<NUM_PLANTS; pl++){
    if(!plantClasses[pl].isSelected()){
      continue;  
    }
    if(parameterType == 0||parameterType == 1||parameterType == 2||parameterType == 5){
        int plantValue = plantClasses[pl].getParameterValue(selectedParameter);
        int plantValueSteps = (plantValue - parameterMin) / (parameterSpan * 1.00) * NUM_STEP_LEDS;
        renderValue((int)plantValueSteps);
        
      }
  }



  if (parameterType == 3) {
    renderSelectionBlink(mainSteps, 2);
  }

  if (parameterType == 0) {
    renderSteps((int)steps);
  }

  if (parameterType == 1) {
    renderMin((int)steps);
  }
  if (parameterType == 2) {
    renderMax((int)steps);
  }

  if (parameterType == 4) {
    renderCount(mainSteps, subSteps);
  }

  if (parameterType == 5) {
    renderSteps((int)parameterVal);
  }


  renderTempoBlink();

  FastLED.show();
}



void renderCount(int mainSteps, int subSteps) {

  for (int i = 0; i < NUM_STEP_LEDS; i++) {
    if (i <= subSteps - 1) {
      stepLeds[i] = countColor;
    }

    if (i == mainSteps && blinkState) {
      stepLeds[i] = white;
    }

  }
}


void renderSteps(int noSteps) {
  stepLeds[noSteps] = white;
}


void renderValue(int noSteps) {
  if (blinkState) {
    stepLeds[noSteps] = valueColor;
  }
}





void renderMin(int noSteps) {

  for (int i = 0; i < NUM_STEP_LEDS; i++) {
    if (i >= noSteps) {
      stepLeds[i] = white;
    }
  }
}



void renderMax(int noSteps) {
  for (int i = 0; i < NUM_STEP_LEDS; i++) {
    if (i <= noSteps) {
      stepLeds[i] = white;
    }
  }
}


void renderSelectionBlink(int stepNo, int parameterResolution) {
  if (blinkState) {
    for (int i = stepNo; i < stepNo + parameterResolution; i++) {
      stepLeds[i] = blinkColor;
    }
  }
}


void renderPlantSelction() {
  for (int pl = 0; pl < NUM_PLANTS; pl++) {
    if (plantClasses[pl].isSelected()) {
      stepLeds[pl * 2] = plantGreen;
      stepLeds[pl * 2 + 1] = plantGreen;
    }
  }
}

void renderTempoBlink() {
  paramLedsRight[0] = CRGB(tempoBlinkValue, tempoBlinkValue, tempoBlinkValue);

}

void renderParameterSelection() {
  for (int i = 0; i < NUM_PARAM_LEDS; i++) {
    if (parameterClasses[selectedParameter].leftLedLight == i) {
      paramLedsLeft[NUM_PARAM_LEDS - i - 1] = orange;
    }
    else {
      paramLedsLeft[NUM_PARAM_LEDS - i - 1] = inactive;
    }

    if (parameterClasses[selectedParameter].rightLedLight == i) {
      paramLedsRight[NUM_PARAM_LEDS - i - 1] = orange;
    }
    else {
      paramLedsRight[NUM_PARAM_LEDS - i - 1] = inactive;
    }
  }
}

void changeBlinkState() {
  blinkState = !blinkState;
}


