#ifndef GLOBALVAR_H
#define GLOBALVAR_H


//#define DEBUG
//#define DEBUG_GRAPH

/* Multiplexer pins */
#define MULTIPLEXER_PIN_A 30
#define MULTIPLEXER_PIN_B 31
#define MULTIPLEXER_PIN_C 32

/* LED pins */
#define PARAMS_PIN_LEFT 2
#define PARAMS_PIN_RIGHT 3
#define STEPS_PIN 4

/* Control input pins */
#define KNOP_LEFT_1 51
#define KNOP_LEFT_2 50
#define KNOP_RIGHT_1 52
#define KNOP_RIGHT_2 53
#define KNOP_RIGHT_BTN 49
#define KNOP_LEFT_BTN 48

/* numbers */
#define NUM_PLANTS 8
#define NUM_PARAMS 7
#define NUM_PARAM_LEDS 8
#define NUM_STEP_LEDS 16
#define MAX_VALUE 127
#define MAX_READING_VALUE 540
#define AUTO_SENS_REACT_TIME 0.000001
#define NUM_SUBDIVISIONS 16
#define NUM_SCALES 16

/* parameter indexes */
#define PLANT_SELECT_PARAM_INDEX 0
#define PLANT_TONE_INDEX 1
#define PLANT_SCALE_INDEX 2
#define TONE_MIN_PARAM_INDEX 3
#define TONE_MAX_PARAM_INDEX 4
#define SUBDIV_PARAM_INDEX 5
#define TEMPO_PARAM_INDEX 6

const float subdivisions[NUM_SUBDIVISIONS] = {
  16.00 / 1.00,
  8.00 / 1.00,
  4.00 / 1.00,
  3.00 / 1.00,
  2.00 / 1.00,
  4.00 / 3.00,
  1.00 / 1.00,
  2.00 / 3.00,
  3.00 / 4.00,
  1.00 / 2.00,
  1.00 / 3.00,
  3.00 / 8.00,
  1.00 / 4.00,
  1.00 / 6.00,
  1.00 / 8.00
};

const int scales[NUM_SCALES][12] =
{
  {0, 2, 4, 5, 7, 9, 11, 12, 0, 0, 0, 0}, // Durskala
  {0, 2, 3, 5, 7, 8, 10, 12, 0, 0, 0, 0}, // Ren mol
  {0, 2, 3, 5, 7, 8, 11, 12, 0, 0, 0, 0}, // Harmonisk mol:
  {0, 2, 3, 5, 7, 9, 11, 12, 0, 0, 0, 0}, // Melodisk mol:
  {0, 2, 4, 7, 9, 0, 0, 0, 0, 0, 0, 0}, // Pentaton dur
  {0, 3,  5, 7, 10, 0, 0, 0, 0, 0, 0, 0}, // Penaton mol
  {0, 3, 5, 6, 7, 10, 0, 0, 0, 0, 0, 0}, // Blues
  {0, 2, 4, 6, 8, 10, 0, 0, 0, 0, 0, 0}, // Heltone
  {0, 2, 3, 5, 7, 9, 10, 0, 0, 0, 0, 0}, // Dorisk (= ren molskala med hævet 6. trin)
  {0, 1, 3, 5, 7, 8, 10, 0, 0, 0, 0, 0}, // Frygisk (= ren molskala med sænket 2. trin)
  {0, 2, 4, 6, 7, 9, 11, 0, 0, 0, 0, 0}, // Lydisk (= durskala med hævet 4. trin)
  {0, 2, 4, 5, 7, 9, 10, 0, 0, 0, 0, 0}, // Mixolydisk (= durskala med sænket 7. trin)
  {0, 1, 3, 5, 6, 8, 10, 0, 0, 0, 0, 0}, // Lokrisk (= ren molskala med sænket 2. og 5. trin)
  {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11}, // Kromatisk
  {0, 1, 3, 4, 6, 7, 9, 10, 0, 0, 0, 0}, // Oktatone:
  {0, 2, 3, 5, 6, 8, 9, 11, 0, 0, 0, 0}, // Hel/halvtoneskala
};

#endif



