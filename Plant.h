#include "Arduino.h"
#include "GlobalVariables.h"

#define NUM_READINGS 1


class Plant
{
  public:
    Plant();
    void doReading();
    bool isSelected();
    void setupPlant(int plantIndex,int analogPin,int parameterValues[NUM_PARAMS]);
    void toogleSelection();
   
    int getParameterValue(int parameterIndex);
    void setParameterValue(int parameterIndex,int value);
    int getPlantIndex();
    int getCurrentTone();
    int getPrevTone();
    int getReadingValue();
    void setPrevTone(int t);
    bool isActive();
 
  private:
    float _autoSensMin=MAX_READING_VALUE;
    float _autoSensMax=MAX_READING_VALUE;
    int _maxReading=0;
    int _minReading=0;
    int _currentReading=0;
    int _readingPin;
    int _parameterValues[NUM_PARAMS];
    int _readings[NUM_READINGS];
    int _readingNo = 0;
    int _currentReadingValue = 0;
    int _readingValue = 0;
    int _prevTone;
    int _plantIndex;
};


Plant::Plant()
{
  
}

int Plant::getPlantIndex(){
  return _plantIndex;
}


int Plant::getPrevTone(){
  return _prevTone;
}

int Plant::getReadingValue(){
  return _readingValue;  
};


int Plant::getParameterValue(int parameterIndex){
  return _parameterValues[parameterIndex];  
};

void Plant::setParameterValue(int parameterIndex,int value){

  #ifdef  DEBUG_MODE
    Serial.print("setting parameter index: ");
    Serial.print(parameterIndex);
    Serial.print(" to: ");
    Serial.print(value);
    Serial.print(" at plant: ");
    Serial.println(_plantIndex); 
  #endif

 
  _parameterValues[parameterIndex] = value;
}

void Plant::setupPlant(int plantIndex,int analogPin,int parameterValues[NUM_PARAMS]){
  _readingPin = analogPin;
  _plantIndex = plantIndex;
  
  for(int i=0; i<NUM_PARAMS; i++){
     _parameterValues[i] =  parameterValues[i];
  }
}


void Plant::setPrevTone(int t){
  _prevTone = t;
}


int Plant::getCurrentTone(){
           int toneValue = map(_readingValue,_autoSensMin,_autoSensMax,_parameterValues[TONE_MIN_PARAM_INDEX],_parameterValues[TONE_MAX_PARAM_INDEX]);

              #ifdef  DEBUG_MODE
              Serial.print("tonevalue:");
              Serial.println(toneValue);
              #endif

          if(toneValue<_parameterValues[TONE_MIN_PARAM_INDEX]){
            toneValue = _parameterValues[TONE_MIN_PARAM_INDEX];
          }
          if(toneValue>_parameterValues[TONE_MAX_PARAM_INDEX]){
            toneValue = _parameterValues[TONE_MAX_PARAM_INDEX];
          }
          
          
         
          int possibleTonesInScale[127];
          int toneNo = 0;
          int maxOctaves = 11;
          int selectedTone = -1;
        
          int curSpan = 127;
          
          for(int o=0; o<maxOctaves; o++){
            for(int t=0; t<12; t++){
                 
                 int possibleTone =  o*12 + _parameterValues[PLANT_TONE_INDEX] + scales[_parameterValues[PLANT_SCALE_INDEX]][t];
                 int diff = abs(toneValue-possibleTone);
                 if(diff<curSpan){
                   selectedTone = possibleTone;
                   curSpan = diff;
                  }
             }               
           }
           /*
           
             
          Serial.print("send midi note on ch: ");
            //Serial.print( p+1);
            Serial.print(" tone: ");
          Serial.println(selectedTone);
              */
          
        
           _maxReading = 0;
           _minReading = 1024;
        
           return selectedTone;
}




void Plant::toogleSelection(){
  if(_parameterValues[PLANT_SELECT_PARAM_INDEX]){
    _parameterValues[PLANT_SELECT_PARAM_INDEX] = 0;
    }
    else{ 
      _parameterValues[PLANT_SELECT_PARAM_INDEX] = 1;
    }
}

bool Plant::isActive(){
  return _currentReadingValue < MAX_READING_VALUE;  
}

bool Plant::isSelected(){
  return _parameterValues[PLANT_SELECT_PARAM_INDEX];  
}

void Plant::doReading()
{
    _currentReadingValue = analogRead(_readingPin);


    

    int diffMin = abs(_autoSensMin-_currentReadingValue);
    
    _autoSensMin = _autoSensMin+ pow(2,diffMin) * AUTO_SENS_REACT_TIME;

    
    if(_currentReadingValue < _autoSensMin){
       _autoSensMin = _currentReadingValue; 
        //_autoSensMin = _autoSensMin-pow(2,diffMin) * AUTO_SENS_REACT_TIME*0.0001;
     } 
     

     
     int diffMax = abs(_currentReadingValue-_autoSensMax);
     
    _autoSensMax = _autoSensMax - pow(2,diffMax) * AUTO_SENS_REACT_TIME;
    
    if(_autoSensMax < _currentReadingValue){
      _autoSensMax = _currentReadingValue; 
    } 
    
    
     
    #ifdef DEBUG_GRAPH
    
    if(isSelected()){

      
    
      Serial.print(_currentReadingValue);
      Serial.print(",");
      Serial.print(_autoSensMin);
      Serial.print(",");
      Serial.print(_autoSensMax);
      Serial.print(",");
      Serial.print(_readingValue);
       Serial.print(",");
    
      
   
    }

    if(_plantIndex == NUM_PLANTS-1){
      
        Serial.println("");
        
    }  

  #endif    
 
  

  
    /*
   Serial.print("plant: ");
   Serial.print(_plantIndex);
   Serial.print(" value: ");
   Serial.println(readingValue);
   */
   if(_currentReadingValue > _maxReading){
     _maxReading = _currentReadingValue;
   }
   if(_currentReadingValue < _minReading){
     _minReading = _currentReadingValue;
   }

   /*
   _readings[_readingNo] = _currentReadingValue;
   _readingNo ++;
   if(_readingNo >= NUM_READINGS){
      _readingNo = 0;
    }
    
     int sum = 0;
      
     for(int r=0; r<NUM_READINGS; r++){
        sum =sum + _readings[r];
     }
     _readingValue = sum/NUM_READINGS;

    */

    _readingValue -= _readingValue / NUM_READINGS;
    _readingValue += _currentReadingValue / NUM_READINGS;

    
}


