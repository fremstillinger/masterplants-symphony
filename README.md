#Masterplants Symphony



##Devices used
Arduino Mega
Seedstudio GSR https://www.seeedstudio.com/s/gsr.html
CD4051BEE4, Multiplexer

##Dependencies
FastLED.h http://fastled.io
Bounce2.h https://github.com/thomasfredericks/Bounce2
Timer.h https://github.com/JChristensen/Timer/blob/master/Timer.h
Rotary.h https://github.com/buxtronix/arduino/blob/master/libraries/Rotary/Rotary.h
MIDI.h https://github.com/FortySevenEffects/arduino_midi_library
