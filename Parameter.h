#include "Arduino.h"
#include "GlobalVariables.h"

// [leftledlight, rightledlight, type (0=step, 1=min,2=max,3=select,4=numeric,5=absolute step),resolution(in steps),default/current value, min, max]


class Parameter
{
  public:
    Parameter();
    int leftLedLight;
    int rightLedLight;
    
    // type (0=step, 1=min,2=max,3=select,4=numeric,5=absolute step)
    int type;
    int resolution;
    int currentValue;
    int minValue;
    int maxValue;
    void Parameter::setupParameter(int llLight,int rlLight,int t,int r,int c,int minVal,int maxVal);
   
  private:
    int _readingPin;
    int _parameterValues[NUM_PARAMS];
    int _readings[NUM_READINGS];
    int _redaingNo = 0;
    int _readingValue = 0;
    int _prevTone;
    int _plantIndex;
};


Parameter::Parameter()
{
  
}

void Parameter::setupParameter(int llLight,int rlLight,int t,int r,int c,int minVal,int maxVal){
  leftLedLight = llLight;
  rightLedLight = rlLight;
    // type (0=step, 1=min,2=max,3=select,4=numeric,5=absolute step)
   type = t;
   resolution = r;
   currentValue = c;
   minValue = minVal;
   maxValue = maxVal;
  
}


